# smart-validator
<p>轻量级服务端校验框架</p>
<p>支持注解、功能齐全、使用简便</p>

## 一、功能简介
主要提供便捷的后台数据校验功能，支持单个字段或参数校验，也支持通过注解校验对象，用法简单。<br>
提供基本的非空、长度、大小等校验方法，也提供一些特殊的正则校验、身份证、电话、邮箱、IP等校验方法。

## 二、用法介绍
目前提供以下校验方法，支持后续持续扩展

  | 注解        | 说明    |
  | :---------: | :------ |
  | NotNull | 非空校验 |
  | Max | 最大值校验 |
  | Min | 最小值校验 |
  | MaxLength | 最大长度校验，支持集合、数组长度校验 |
  | MinLength | 最大长度校验，支持集合、数组长度校验 |
  | IdCard | 身份证校验 |
  | Email | 邮箱格式校验 |
  | Phone | 手机号校验 |
  | IP | IP地址校验 |
  | Chinese | 中文校验 |
  | English | 英文校验 |
  | Regex | 自定义正则校验 |
  | Date | 日期格式校验 |

### 1. 单个参数验证
```java
ValidateUtils.is("a").notNull();
 
ValidateUtils.is("test").maxLength(20).minLength(4);
 
ValidateUtils.is(50).min(20).max(60);
```

通过and()支持连写（连写直接切换校验对象）

```java
ValidateUtils.is("a").notNull().and("test").maxLength(20).minLength(4).and(50).min(20).max(60);
```
支持自定义错误信息

```java
ValidateUtils.is("test").maxLength(20,"最大长度不能超过20个字").minLength(4,"最小长度不能少于4个字");
```
### 2. 校验整个对象（通过注解）
在类的属性上定义注解，同时支持自定义错误信息
```java
public class User {

    @NotNull(msg = "姓名不能为空")
    @MaxLength(value = 20,msg = "姓名不能超过20个字")
    private String name;

    private Date birthday;

    @IdCard
    private String idcard;

    @Max(30)
    @Min(12)
    private int age;

    @Email
    @MaxLength(50)
    private String email;

    @Phone
    private String phone;

    @Regex("[1-9]([0-9]{5,11})")
    private String qq;
    
    //get... set..
}
```

然后调用ValidateUtils.check()方法即可

```java
try {
    //....
    ValidateUtils.check(user);
    //.....
}catch (ParamsException e){
    throw e;
}catch (Exception e){
    //...
}
```

同样支持连写

```java
ValidateUtils.check(user).and("2017-06-05").date("yyyy-MM-dd");
```

### 3.校验不通过时处理
校验不通过会抛出ParamsException（运行时异常）<br>

使用时一般不需要特殊处理，由于后台校验是安全性校验，一般用于拦截非法操作，所以不用友好提示，所以推荐不做任何捕获或者特殊处理，如外层有catch，建议单独捕获后向上抛出。<br>
如果想做异常捕获，也可以自行在代码中添加try/catch（不推荐），或者添加全局的拦截器捕获该类异常。<br>


### 三、部署说明
获取最新版本的jar包即可-[下载地址](https://gitee.com/fetech-framework/smart-validator/releases/v1.1.0)，然后添加进项目的依赖库中。<br>
项目使用maven构建，也可以下载源码自行编译<br>

#### 第三方库依赖如下，一般项目都有使用
```xml
<properties>
    <slf4j.version>1.7.13</slf4j.version>
    <commons.lang.version>2.6</commons.lang.version>
    <commons.collections.version>3.2.2</commons.collections.version>
    <commons.beanutils.version>1.9.3</commons.beanutils.version>
</properties>

<dependencies>
    <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-api</artifactId>
        <version>${slf4j.version}</version>
    </dependency>
    <dependency>
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-log4j12</artifactId>
        <version>${slf4j.version}</version>
    </dependency>
    <dependency>
        <groupId>commons-lang</groupId>
        <artifactId>commons-lang</artifactId>
        <version>${commons.lang.version}</version>
    </dependency>
    <dependency>
        <groupId>commons-collections</groupId>
        <artifactId>commons-collections</artifactId>
        <version>${commons.collections.version}</version>
    </dependency>
    <dependency>
        <groupId>commons-beanutils</groupId>
        <artifactId>commons-beanutils</artifactId>
        <version>${commons.beanutils.version}</version>
    </dependency>
</dependencies>
```

### 欢迎使用其他系列产品
melon-idfactory [传送门](https://gitee.com/fetech-platform/melon-idfactory)